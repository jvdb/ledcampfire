#ifndef	FIRE_H
#define	FIRE_H

#ifdef	FIRE_TEST
#include <stdint.h>
#endif

#define W 36
#define H 20

extern uint8_t plane_output[W][H];
extern uint8_t sparks_height[W];
extern uint8_t sparks_intensity[W];
extern uint8_t sparks_speed[W];
volatile extern uint32_t intensity;
volatile extern uint32_t intensity_min;

void fire_recalc(void);
uint8_t i_s(uint8_t scale);
void intensity_increase(void);
void intensity_set(uint8_t);
void intensity_set_min(uint8_t);

#endif	// FIRE_H
