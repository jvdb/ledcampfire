// arduino IDE compat vim: expandtab sw=2 ts=2:
#ifdef  FIRE_TEST
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <inttypes.h>
#include <math.h>

#include "fire.h"

#define BUTTON_FILE_PATH  "./buttonpress"

char asciify_lut[] = { ' ', '.', '-', '=', 'x', '%', '@', '#' };
char asciify(uint8_t val)
{

  return asciify_lut[val/(256/sizeof (asciify_lut))];

}

int main(int argc, char** argv)
{

  struct stat button_stat;

  while (1) {

    usleep(20*1000);

    // if the buttonpres file exists, remove it and simulate
    // a button press
    if (stat(BUTTON_FILE_PATH, &button_stat) == 0) {
      intensity_increase();
      unlink(BUTTON_FILE_PATH);
    }

    fire_recalc();

    printf("intensity=%u", intensity);
    printf("\nheight=   ", intensity);
    for (int x = 0; x < W; x++) {
      printf(" %03u", sparks_height[x]);
    }
    printf("\nspeed=    ");
    for (int x = 0; x < W; x++) {
      printf(" %03u", sparks_speed[x]);
    }
    printf("\nintensity=");
    for (int x = 0; x < W; x++) {
      printf(" %03u", sparks_intensity[x]);
    }
    printf("\n");

    printf("-----\n");
    for (int y = H-1; y >= 0; y--) {
      putchar('|');
      for (int x = 0; x < W; x++) {
        putchar(asciify(plane_output[x][y]));
      }
      putchar('|');
      putchar('\n');
    }
    printf("-----\n");

  }

  return 0;

}
#endif  // FIRE_TEST
