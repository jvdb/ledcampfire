A LED campfire.
===============

A nice warm campfire!

When starting, it provides a nice low burn with the occasional spark. Throw on a log (with the button) to generate lots of sparks and burn a bit brighter! A log takes about half an hour to burn away, max 4 logs can be thrown onto the fire.

Usage
-----

You will need:

* an ESP8266 board (f.e. a NodeMCU)
* an addressable RGB LED string compatible with FastLED (f.e. a 5M/300LED WS2812b string)
* an momentary push-button
* an Arduino IDE (with FastLED loaded)

Open the .ino file in the Arduino IDE, connect DATA_PIN, BUTTON_PIN (NodeMCU example pins given), and adapt NUM_LEDS and the FastLED.addLeds() call to your LED string. Set TESTING_PATTERN to 1. Flash. The bottom of each flame will have a red LED burning, the top a blue one. Build. Flash again with TESTING_PATTERN set to 0.

Hacking
-------

`fire.cpp` does the heavy lifting, and a console port lives in `fire_test.cpp`, you can run `make -f Makefile.test` to build a `fire` binary which dumps a nice ascii rendition to stdout.

`touch ./buttonpress` to simulate a button press!
