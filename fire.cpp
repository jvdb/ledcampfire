// arduino IDE compat vim: expandtab sw=2 ts=2:
#ifdef	FIRE_TEST
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdint.h>
#define	RND (random() & 0xff)
#else
#include <Arduino.h>
#define RND (random(0, 255) & 0xff)
#endif

#include "fire.h"

// spark state, a spark s exists if _intensity[x] is nonzero
uint8_t sparks_height[W];     // in pct of H, static spark life
uint8_t sparks_intensity[W];  // spark size, declines over height
uint8_t sparks_speed[W];      // speed in height increments

// just the fire, not sparks
uint8_t plane_fire[W][H];

// all effects combine here before rendering
uint8_t plane_output[W][H];

// intensity represents the intensity of the fire over time
// (gets decremented each _recalc()), which can be increased
// via increase_intensity() (to _MAX in _N_STEPS), basing it
// on FPS yields a timeframe over which it'll return to a
// natural https://www.youtube.com/watch?v=PEi3qtMtKgU
#define INTENSITY_MAX (30*60*50)
#define INTENSITY_N_STEPS 3
volatile uint32_t intensity = INTENSITY_MAX;

// floor of intensity, for knob twiddlin'
volatile uint32_t intensity_min = 0;

// bumped on each intensity_increase(), used as an indicator
// to generate a flare/sparks, reset each _recalc()
volatile char intensity_bump;

// current intensity scaled down to scale
uint8_t i_s(uint8_t scale)
{

  return (uint8_t) ((scale * intensity) / INTENSITY_MAX);

}

// bump intensity by _MAX/_N_STEPS, will cap at _MAX
void intensity_increase(void)
{

  if (intensity < (INTENSITY_MAX - INTENSITY_MAX/INTENSITY_N_STEPS))
    intensity += INTENSITY_MAX/INTENSITY_N_STEPS;
  else
    intensity = INTENSITY_MAX;

  intensity_bump++;

}

// statically set intensity (0-(2*8-1))
void intensity_set(uint8_t i)
{

  intensity = i * (INTENSITY_MAX / 0xff);

}

// statically set min intensity (0-(2*8-1))
void intensity_set_min(uint8_t i)
{

  intensity_min = i * (INTENSITY_MAX / 0xff);

}

void fire_recalc(void)
{

  uint8_t i_100 = i_s(99);
  uint8_t i_255 = i_s(255);
  float i_1 = i_100 * 0.01;

  // an flare happens only sporadically and is a single
  // uproar of flames (always happens on an intensity_bump)
  char flare = (RND > 0xf8) ? 1 : 0;
  if (intensity_bump)
    flare = 1;

  intensity = (intensity > intensity_min) ? intensity - 1 : intensity_min;

  // normal burn

  // randomize bottom line
  for (int x = 0; x < W; x++) {
    plane_fire[x][0] = 128 + (RND-127); // force at least half lit pixels
  }

  // work down, each pixel gets added with the one below,
  // then damped by an intensity-based amount (numbers found
  // empirically by staring), and less so during a flare
  // (TODO not happy with this, a flare here at high intensity
  // results in the entire plane flashing up, must be better)
  for (int y = H-1; y > 0; y--) {
    for (int x = 0; x < W; x++) {
      plane_fire[x][y] += plane_fire[x][y-1];
      if (!flare) {
        plane_fire[x][y] *= 0.44 + i_1 * 0.05;  // 0.44 - 0.49
      } else {
        plane_fire[x][y] *= 0.90 + i_1 * 0.10;  // 0.90 - 1.00
      }
    }
  }

  // copy over into output plane, overwrite prev
  for (int x = 0; x < W; x++) {
    for (int y = 0; y < H; y++) {
      plane_output[x][y] = plane_fire[x][y];
    }
  }

  // amp up the bottom two rows in the output, staring
  // indicated this to provide a less flickering base
  for (int x = 0; x < W; x++) {
    plane_output[x][0] = (plane_output[x][0] < 127) ? plane_output[x][0]+127 : plane_output[x][0];
    plane_output[x][1] = (plane_output[x][1] < 127) ? plane_output[x][1]+64 : plane_output[x][1];
  }

  // sparks

  // check for new sparks, for each non-running one
  // (sparks_intensity[x] == 0) consider starting a new
  // spark (with a higher chance during a flare)
  for (int x = 0; x < W; x++) {
    if (sparks_intensity[x] == 0) {
      uint8_t r = RND;
      if (intensity_bump || (flare && (r > (250-i_s(64)))) || (r > (254 - i_s(10)))) {
        sparks_intensity[x] = RND;
        sparks_height[x] = 1;
        sparks_speed[x] = 3 + RND/32; // 2 - 9
      }
    }
  }

  // blit spark pixels given state
  for (int w = 0; w < W; w++) {
    if (sparks_intensity[w] == 0)
      continue;
    uint8_t h = (H * sparks_height[w]) / 100;
    uint8_t val = sparks_intensity[w];
    // spread is the radius of the spark
    uint8_t spread = val / 48;
    if (!spread)
      spread = 1;
    uint8_t decay = val / spread;
    // draw ball of pixel representing spark, figure out
    // values based on distance over spread from center,
    // nets us one quadrant of the ball
    for (int sx = 0; sx < spread; sx++) {
      for (int sy = 0; sy < spread; sy++) {
        int v = val - (sqrt(sx*sx + sy*sy) * decay);
        if (v < 0)
          // dont underflow
          v = 0;
        int x;
        int y;
        // loop over four quadrants, add spark values
        // to output, modulo plane width
        for (int dir_x = -1; dir_x < 2; dir_x += 2) {
          for (int dir_y = -1; dir_y < 2; dir_y += 2) {
            x = ((w - dir_x * sx) + W) % W;
            y = h - dir_y * sy;
            if (y < 0 || y >= H)
              // outside of plane
              continue;
            plane_output[x][y] += v;
            if (plane_output[x][y] < v)
              // don't overflow
              plane_output[x][y] = 0xff;
          }
        }
      }
    }

    // if the spark's too high or too diminished, kill it
    if (((h - spread) >= H) || sparks_intensity[w] <= 10) {
      sparks_intensity[w] = 0;
      sparks_height[w] = 0;
      sparks_speed[w] = 0;
    } else {
      sparks_height[w] += sparks_speed[w];
      sparks_intensity[w] -= 6; // staring
    }

  }

  intensity_bump = 0;

}
