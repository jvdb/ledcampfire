// arduino IDE compat vim: expandtab sw=2 ts=2:
//
// given a WS2812B string and a micro, simulate a campfire
//
// currently runs on an ESP8266 or ESP32, could run on an
// atmega328 if one drops some effects
//
// Jef Van den broeck, 2018, jef.vdb@gmail.com
// Gamma correction LUT from Anthony Liekens
// GPL v2

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define FASTLED_ESP8266_RAW_PIN_ORDER
// removes flicker on my ESP8266/WS2812B
#define FASTLED_ALLOW_INTERRUPTS 0
#include <FastLED.h>
#include "fire.h"

// optionally show a testing pattern which shows the bottom
// (red) and top (blue) of each arm, 0 to disable, 1 to enable
#define TESTING_PATTERN 0

#define NUM_LEDS    300
#define NUM_STRIPS  2
#define DATA_PIN_STRIP_1    16//17   // 0 is D3 on nodemcu
#define DATA_PIN_STRIP_2    17
#define BUTTON_PIN  2//16   // 2 is D4 on nodemcu
#define POT_PIN     A0
#define FPS         30

CRGB leds[NUM_LEDS * NUM_STRIPS];

Adafruit_SSD1306 display(128, 64, &Wire, -1, 800000);

// definition of arms, with:
// - offset the pixel number of lowest point of that arm
// - length the length of that arm (till top)
// - direction is >0 if physically successive leds in this
//   arm go up in direction, or <0 of they go down
// - map says which column of pixels in the imaginary screen
//   map to which arm (in case they interlace vertically)
//
// the map basically means "the x-th column of our imaginary
// fire is represented by this stretch/arm of the LED string",
// and can be found by stringing it all up and looking at which
// arm physically represents which column of the screen
//
//
// as an example; suppose our fire is made out of 3 loops of 20
// pixels each, so 6 arms of 10 pixels each:
//  arm 1 - the left part of loop 1, starts at pixel 0, goes up
//  arm 2 - the right part of loop 1, starts at pixel 10, goes down
//  arm 3 - the left part of loop 2, starts at pixel 20, goes up
//  arm 4 - the right part of loop 2, starts at pixel 30, goes down
//  ...
// so:
//  arm_offsets is { 0, 10, 20, 30, 40, 50 }
//  arm_directions is { 1, -1, 1, -1, 1, -1 }
//  arm_lengths is { 10, 10, 10, 10, 10, 10 }
//
// and suppose we overlap those like rings in a chain, we build the
// map by looking at the very left and noting down which exact arm
// actually should display that "column" of our "screen":
//  column 1 - the left part of loop 1, so arm 1
//  column 2 - the left part of loop 2, so arm 3
//  column 3 - the right part of loop 1, so arm 2
//  column 4 - the left part of loop 3, so arm 5
//  column 5 - the right part of loop 2, so arm 4
//  column 6 - the right part of loop 3, so arm 6
// the map would then be { 1, 3, 2, 5, 4, 6 }
// (and this gets better when the loops are placed in a circle and
// the last ones overlap with the first)

// my build has 12 loops of 30 LEDs each (so, 24 arms each 15
// tall), surrounding 6 loops of 40 LEDs (adding 12 arms each
// 20 tall), totalling 36 arms
#define N_ARMS 36
uint16_t arm_offsets[] = { 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210, 225, 240, 255,
270, 285, 300, 315, 330, 345,360, 380, 400, 420, 440, 460, 480, 500, 520, 540, 560, 580 };
int8_t arm_directions[] = { 1, -1, 1, -1, 1, -1,  1,  -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1,
1, -1, 1, -1, 1, -1,  1,  -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1};
uint8_t arm_lengths[] = { 15, 15, 15, 15, 15, 15, 15, 15,  15,  15,  15,  15,  15,  15,  15,  15,  15,  15,
15, 15, 15, 15, 15, 15, 20, 20,  20,  20,  20,  20,  20,  20,  20,  20,  20,  20 };
uint8_t arm_map[] = { 0, 25, 1, 2, 26, 3, 4, 27, 5, 6, 28, 7, 8, 29, 9, 10, 30, 11, 12, 31, 13, 14, 32, 15, 16, 33, 17,
18, 34, 19, 20, 35, 21, 22, 24, 23 };

/*
uint16_t arm_offsets[] = { 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 200, 220, 240, 260, 280,
300, 315, 330, 345, 360, 375, 390, 405, 420, 435, 450, 465, 480, 500, 520, 540, 560, 580 };
int8_t arm_directions[] = { 1, -1, 1, -1, 1, -1,  1,  -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1,
1, -1, 1, -1, 1, -1,  1,  -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1,  1,   -1};
uint8_t arm_lengths[] = { 15, 15, 15, 15, 15, 15, 15, 15,  15,  15,  15,  15,  20,  20,  20,  20,  20,  20,
15, 15, 15, 15, 15, 15, 15, 15,  15,  15,  15,  15,  20,  20,  20,  20,  20,  20 };
uint8_t arm_map[] = { 0, 12, 1, 2, 13, 3, 4, 14, 5, 6, 15, 7, 8, 16, 9, 10, 17, 11,
18, 30, 19, 20, 31, 21, 22, 32, 23, 24, 33, 25, 26, 34, 27, 28, 35, 29 };
*/
// colors are by definition static, the lowest pixels are red and
// the highest are yellow, the following define is multiplied by
// each pixel's vertical offset (so 0 to arm_lengths[that_arm]) to
// map into the hue in the HSV colorscale (a byte, we need values
// 0 (red) to ~70 (where yellow turns into green))
// this should be static, but in practice different LED strips
// provide different results, so we have a define for that multi-
// plication constant here, if your top pixels are red/orange try
// a higher value, if it's green or worse try a lower value
// (see https://github.com/FastLED/FastLED/wiki/FastLED-HSV-Colors )
#define PIXEL_VERT_TO_HUE_MULT 2.5//1.9

// thx Anthony Liekens for his gamma correction LUT
const uint8_t gammaCorrection[ 256 ] = {
  0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3,
  3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 7,
  7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 9, 10, 10, 10,
  10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14,
  14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18,
  18, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 22, 22, 22, 22, 23,
  23, 23, 24, 24, 24, 24, 25, 25, 25, 26, 26, 26, 27, 27, 27, 28,
  28, 28, 28, 29, 29, 29, 30, 30, 30, 31, 31, 31, 32, 32, 32, 33,
  33, 34, 34, 34, 35, 35, 35, 36, 36, 36, 37, 37, 38, 38, 38, 39,
  39, 39, 40, 40, 41, 41, 41, 42, 42, 43, 43, 44, 44, 44, 45, 45,
  46, 46, 47, 47, 47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53,
  53, 54, 54, 55, 55, 56, 56, 57, 58, 58, 59, 59, 60, 60, 61, 62,
  62, 63, 63, 64, 65, 65, 66, 67, 67, 68, 69, 69, 70, 71, 72, 72,
  73, 74, 75, 76, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
  88, 89, 90, 91, 92, 93, 95, 96, 97, 99, 100, 102, 103, 105, 107, 108,
  110, 112, 114, 117, 119, 122, 124, 127, 131, 135, 139, 144, 150, 158, 171, 255
};

void setup() {

  FastLED.addLeds<NEOPIXEL, DATA_PIN_STRIP_1>(leds, 0, NUM_LEDS);
  FastLED.addLeds<NEOPIXEL, DATA_PIN_STRIP_2>(leds, NUM_LEDS, NUM_LEDS);

  Serial.begin(115200);
  Serial.println("booting");
//#define I2C_FREQ 1000000
//    Wire.setClock(I2C_FREQ); // Wire.setClock(800000);
     display.begin(SSD1306_SWITCHCAPVCC, 0x3c);
//     Wire.setClock(I2C_FREQ);
/*
  display.begin(SSD1306_SWITCHCAPVCC, 0x7a); //0x3c);

  Wire.setClock(800000);
  */
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);

  
Serial.println("display done");
  intensity_set(0);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(BUTTON_PIN, intensity_increase_button, FALLING);
  randomSeed(analogRead(0));

}

unsigned long intensity_increase_last_press = 0;

void intensity_increase_button(void){
  unsigned long now = millis();

  if ((now - intensity_increase_last_press) > 200) {
    intensity_increase();
    intensity_increase_last_press = now;
  }
}

static int testing_dot = 0;
unsigned long last_loop_millis = 0;

void loop() {

  unsigned long this_loop_start = millis();
  
  int pot = 0;
  
  pot = 0;//analogRead(POT_PIN);
  //intensity_set_min((pot - 1) / 4);

  if (!TESTING_PATTERN) {
    fire_recalc();

    // map fire plane to arms
    for (int i = 0; i < N_ARMS; i++) {
      int arm = arm_map[i];
      int arm_length = arm_lengths[arm];
      int arm_offset = arm_offsets[arm];
      int arm_direction = arm_directions[arm];

      for (int p = 0; p < arm_length; p++) {
        int h = p*PIXEL_VERT_TO_HUE_MULT; // see notes about colors
        int s = 255;      // full-blast saturation
        int v = plane_output[i][p];
        if (arm_direction > 0) {
          leds[arm_offset + p].setHSV(h, s, v);
        } else {
          leds[arm_offset + (arm_length - p - 1)].setHSV(h, s, v);
        }
      }
    }

    // apply gamma correction
    for (int i = 0; i < NUM_LEDS; i++) {
      leds[i].red = gammaCorrection[leds[i].red];
      leds[i].green = gammaCorrection[leds[i].green];
      leds[i].blue = gammaCorrection[leds[i].blue];
    }

  } else {

    for (int i = 0; i < (NUM_LEDS * NUM_STRIPS); i++)
      leds[i] = CRGB::Black;
    for (int i = 0; i < N_ARMS; i++) {
      int arm = arm_map[i];
      int arm_length = arm_lengths[arm];
      int arm_offset = arm_offsets[arm];
      int arm_direction = arm_directions[arm];

      if (arm_direction > 0) {
        leds[arm_offset] = CRGB::Red;
        leds[arm_offset + arm_length - 1] = CRGB::Blue;
      } else {
        leds[arm_offset + arm_length - 1] = CRGB::Red;
        leds[arm_offset] = CRGB::Blue;
      }
    }

    leds[testing_dot++] = CRGB::Green;
    if (testing_dot == (NUM_LEDS * NUM_STRIPS))
      testing_dot = 0;

  }
  FastLED.show();

  display.clearDisplay();
  for (int x = 0; x < W; x++) {
    for (int y = 0; y < H; y++) {
      char v = plane_output[x][y] / 4;
      if (v > 0x10)
        display.drawPixel(x*3, (H-y)*2, WHITE);
      if (v > 0x20)
        display.drawPixel(x*3+1, (H-y)*2, WHITE);
      if (v > 0x30)
        display.drawPixel(x*3, (H-y)*2+1, WHITE);
    }
  }
  display.setCursor(0, 43);
  
  display.printf("fps: %02d", int(1000.0 / last_loop_millis));
  display.setCursor(0, 53);
  display.printf("logs: %d/3", (i_s(6)+1)/2);
  display.display();
  
  int naptime = (1000/FPS) - (millis() - this_loop_start);
  if (naptime > 0)
    delay(naptime);

  // figure out how long this loop took, for displaying
  last_loop_millis = millis() - this_loop_start;

}
