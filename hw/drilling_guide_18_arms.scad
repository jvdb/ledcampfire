guide_r = 5/2;
guide_l = 30;	// approx, we'll cut some to flatten the bottom
guide_t = 2;	// wall thickness

pin_r = 2/2;

n_inner = 3;
n_outer = 6;

r_inner = 25;
r_outer = 30;

angle_inner = 10;	// from z axis
angle_outer = 35;

$fn = 20;

module guides(guide_r, n, angle, r, angle_offset=0) {
	for (i = [0:(n-1)]) {
		// read inside-out
		
		rotate([0, 0, 360/n*i + angle_offset])
			// offset this guide to its place on the guide circle
			translate([r, 0, 0])
				// rotate to this guide's desired angle
				rotate([0, angle, 0])
					// move upwards so ass end is at z~=0
					translate([0, 0, guide_l/2])
						// a guide
						cylinder(r=guide_r, h=guide_l, center=true);
	}
}

// figure out how much, given the angles, we lower it all so all guide holes completely touch z=0
max_angle = max([angle_inner, angle_outer]);
z_off = sin(max_angle) * guide_r;

difference() {
	union() {
		// the guides
		translate([0, 0, -z_off]) {
			guides(guide_r+guide_t, n_inner, angle_inner, r_inner);
			guides(guide_r+guide_t, n_outer, angle_outer, r_outer, 360/n_outer/2);
		}
		// a bottom plate flat at z=0
		translate([0, 0, guide_t/2])
			cylinder(r=r_outer+guide_r+guide_t, h=guide_t, center=true);
	}
	// guide contents
	translate([0, 0, -z_off]) {
		guides(guide_r, n_inner, angle_inner, r_inner);
		guides(guide_r, n_outer, angle_outer, r_outer, 360/n_outer/2);
	}
	// a pin to pin it all
	translate([0, 0, guide_t/2]) cylinder(r=pin_r, h=guide_t, center=true);
	// everything below z=0
	_floor = [1000, 1000, 1000];
	translate([0, 0, -_floor[2]/2]) cube(_floor, center=true);
}
